package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Activity2 {
    public static void main(String[] args){
        int[] prime = {2,3,5,7,11};
        System.out.println("The first prime number is: " + prime[0]);
        System.out.println("The second prime number is: " + prime[1]);
        System.out.println("The third prime number is: " + prime[2]);
        System.out.println("The fourth prime number is: " + prime[3]);
        System.out.println("The fifth prime number is: " + prime[4]);

        ArrayList<String> friends = new ArrayList<>(Arrays.asList("John", "Jane", "Chloe", "Zoey"));
        System.out.println("My friends are: " + friends);

        HashMap<String, Integer> inventory = new HashMap<>(){
                {
                    put("toothpaste",15);
                    put("toothbrush", 20);
                    put("soap", 12);
                }
        };

        System.out.println("Our current inventory consists of : " + inventory);
    }
}
